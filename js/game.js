var GameState = {
    preload: function(){
        game.load.image('block1','assets/item/blue block.png');
        game.load.image('block2','assets/item/green block.png');
        game.load.image('block3','assets/item/lightblue block.png');
        game.load.image('block4','assets/item/orange block.png');
        game.load.image('block5','assets/item/purple block.png');
        game.load.image('block6','assets/item/red block.png');
        game.load.image('block7','assets/item/yellow block.png');
        game.load.image('floor','assets/item/ghost block.png');
        game.load.image('blocks1','assets/item/blue_block.png');
        game.load.image('blocks2', 'assets/item/green_block.png');
        game.load.image('blocks3', 'assets/item/lightblue_block.png');
        game.load.image('blocks4', 'assets/item/orange_block.png');
        game.load.image('blocks5', 'assets/item/purple_block.png');
        game.load.image('blocks6', 'assets/item/red_block.png');
        game.load.image('blocks7', 'assets/item/yellow_block.png');
        //game.load.spritesheet('gabulin','assets/monster/slime-Sheet.png',32,25);
        game.load.image('gabulin', 'assets/monster/mon_052.gif');
        game.load.image('player','assets/monster/mon_283.gif');
        game.load.image('bg','assets/item/bg.png');
        game.load.image('rpg_bg','assets/2.gif');
    },
    create: function(){
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.renderer.renderSession.roundPixels = true;
        //block
        this.background = game.add.sprite(0,0,'bg');

        this.block_array = ['block1', 'block2', 'block3', 'block4', 'block5', 'block6', 'block7'];
        this.check_blcok = [0,1,2,3,4,5,6];
        this.createBlock();
        this.falling = game.time.events.loop(1000, this.BlockFalling, this);
        //floor
        this.floor = game.add.group();
        this.floor.enableBody = true;
        for(var i=0;i<10;i++){
            var floor_block = game.add.sprite(i*18,18*29,'floor',0,this.floor);
            floor_block.body.immovable = true;
        }

        this.map = new Array(30);
        for(var t=0;t<29;t++){
            this.map[t] = [0,0,0,0,0,0,0,0,0,0];
        }
        this.map[29] = [1,1,1,1,1,1,1,1,1,1];
        
        this.obstacle = new Array(30);
        for (var t = 0; t < 30; t++) {
            this.obstacle[t] = new Array(10);
        }
        //the keyboard control
        this.control = {
            left: game.input.keyboard.addKey(Phaser.Keyboard.LEFT),
            right: game.input.keyboard.addKey(Phaser.Keyboard.RIGHT),
            down: game.input.keyboard.addKey(Phaser.Keyboard.DOWN),
            z: game.input.keyboard.addKey(Phaser.Keyboard.Z),
            x: game.input.keyboard.addKey(Phaser.Keyboard.X),
            space: game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR)
        };
        //use onDown to trigger (isDown will trigger function repeatedly)
        this.control.space.onDown.add(this.BlockControlDrop,this);
        this.control.left.onDown.add(this.BlockControlLeft,this);
        this.control.right.onDown.add(this.BlockControlRight,this);
        this.control.z.onDown.add(this.BlockContorlRotate,this);
        this.control.x.onDown.add(this.BlockContorlRotateCounterwise,this);
        this.control.down.onDown.add(this.BlockControlDown,this);
       
        this.stage_now = 1;
        
        this.rpg_bg = game.add.sprite(180,0,'rpg_bg');
        //player 
        this.player = game.add.sprite(200,177,'player');
        this.player.level = 1;
        this.player.life = 15;
        //monster create
        this.gabulin = game.add.sprite(450,210,'gabulin');
        this.gabulin.scale.setTo(-1,1);
        this.gabulin.life = 10;
       //this.gabulin.anchor.setTo(0.5,0.5);
        if(this.stage_now == 1){
            game.time.events.loop(5000,this.MonsterAutoAttack,this);
        }
    },
    update: function(){
        this.checkBounds();
        this.checkPosition();
    },
    checkBounds: function(){  
        if(this.new_block.onLand){
            var lose = false;
            this.new_block.forEach(function (child) {
                /*console.log((child.position.y + this.new_block.position.y) / 18);
                console.log((child.position.x + this.new_block.position.x) / 18);*/
                var y = (child.position.y + this.new_block.position.y) / 18;
                var x = (child.position.x + this.new_block.position.x) / 18;
                child.position.y = y * 18;
                if(y < 0){
                    this.LoseGame();
                    lose = true;
                    return;
                }
                this.map[y][x] = 1;
                this.obstacle[y][x] = child;
            }, this);
            this.new_block.position.y = 0;
            if(this.check_block.length!=0){
                this.check_block.kill();
            }
            //console.log(this.obstacle);
            if(!lose){
                this.checkClear();
                game.time.events.pause(this.falling);
                this.createBlock();
                game.time.events.resume(this.falling);
            }
        }
    },
    createBlock: function(){
        //block rule
        this.check_block = game.add.group();
        this.number = game.rnd.pick(this.check_blcok);
        var index = this.check_blcok.indexOf(this.number);
        this.check_blcok.splice(index,1);
        if(this.check_blcok.length == 0)
            this.check_blcok = [0, 1, 2, 3, 4, 5, 6];
        //console.log(this.check_blcok);
        this.new_block = game.add.group();
        this.new_block.enableBody = true;
        //setting the block's shape
        this.createShape(this.number);
        //other setting 
        this.new_block.position.x = 90;
        this.new_block.position.y = 0;
        this.new_block.onLand = false;
    },
    createShape: function(type){
        switch(type){
            case 0:
                game.add.sprite(0, 0, this.block_array[type], 0, this.new_block);
                game.add.sprite(-18, 0, this.block_array[type], 0, this.new_block);
                game.add.sprite(-18, -18, this.block_array[type], 0, this.new_block);
                game.add.sprite(18, 0, this.block_array[type], 0, this.new_block);
                break;
            case 1:
                game.add.sprite(0, 0, this.block_array[type], 0, this.new_block);
                game.add.sprite(0, -18, this.block_array[type], 0, this.new_block);
                game.add.sprite(-18, -18, this.block_array[type], 0, this.new_block);
                game.add.sprite(18, 0, this.block_array[type], 0, this.new_block);
                break;
            case 2:
                game.add.sprite(0, 0, this.block_array[type], 0, this.new_block);
                game.add.sprite(0, -18, this.block_array[type], 0, this.new_block);
                game.add.sprite(0, -36, this.block_array[type], 0, this.new_block);
                game.add.sprite(0, 18, this.block_array[type], 0, this.new_block);
                break;
            case 3:
                game.add.sprite(0, 0, this.block_array[type], 0, this.new_block);
                game.add.sprite(-18, 0, this.block_array[type], 0, this.new_block);
                game.add.sprite(18, -18, this.block_array[type], 0, this.new_block);
                game.add.sprite(18, 0, this.block_array[type], 0, this.new_block);
                break;
            case 4:
                game.add.sprite(0, 0, this.block_array[type], 0, this.new_block);
                game.add.sprite(-18, 0, this.block_array[type], 0, this.new_block);
                game.add.sprite(0, -18, this.block_array[type], 0, this.new_block);
                game.add.sprite(18, 0, this.block_array[type], 0, this.new_block);
                break;
            case 5:
                game.add.sprite(0, 0, this.block_array[type], 0, this.new_block);
                game.add.sprite(-18, 0, this.block_array[type], 0, this.new_block);
                game.add.sprite(0, -18, this.block_array[type], 0, this.new_block);
                game.add.sprite(18, -18, this.block_array[type], 0, this.new_block);
                break;
            case 6:
                game.add.sprite(0, 0, this.block_array[type], 0, this.new_block);
                game.add.sprite(-18, 0, this.block_array[type], 0, this.new_block);
                game.add.sprite(-18, -18, this.block_array[type], 0, this.new_block);
                game.add.sprite(0, -18, this.block_array[type], 0, this.new_block);
                break;
        }
    },
    BlockControlLeft: function(){
        var canLeft = true;
        this.new_block.forEach(function(child){
            var x = (child.position.x + this.new_block.position.x ) / 18;
            var y = (child.position.y + this.new_block.position.y) / 18;
            if (y < 0) y = 0;
            if(x == 0){
                canLeft = false;
            }
            if(this.map[y][x-1]==1){
                canLeft = false;
            }
        },this);

        if(canLeft) this.new_block.position.x -= 18;
    },
    BlockControlRight: function () {
        var canRight = true;
        this.new_block.forEach(function (child) {
            var x = (child.position.x + this.new_block.position.x) / 18;
            var y = (child.position.y + this.new_block.position.y) / 18;
            if(y<0) y = 0;
            if (x == 9) {
                canRight = false;
            }
            if(this.map[y][x+1]==1){
                canRight = false;
            }
        },this);

        if(canRight) this.new_block.position.x += 18;
    },
    BlockContorlRotate: function(){
        var canrotate = true;
        this.new_block.forEach(function (child) {
            var x = (-child.position.y + this.new_block.position.x) / 18;
            if(x<0 || x>9){
                canrotate = false;
            }
        }, this);
        if(canrotate){
            this.new_block.forEach(function (child) {
                var swap = child.position.x;
                child.position.x = -child.position.y;
                child.position.y = swap;
            }, this);
            this.check_block.forEach(function (child) {
                var swap = child.position.x;
                child.position.x = -child.position.y;
                child.position.y = swap;
            }, this);
        }
    },
    BlockContorlRotateCounterwise: function(){
        var canrotate = true;
        this.new_block.forEach(function (child) {
            var x = (child.position.y + this.new_block.position.x) / 18;
            if (x < 0 || x > 9) {
                canrotate = false;
            }
        }, this);
        if (canrotate) {
            this.new_block.forEach(function (child) {
                var swap = child.position.x;
                child.position.x = child.position.y;
                child.position.y = -swap;
            }, this);
            this.check_block.forEach(function (child) {
                var swap = child.position.x;
                child.position.x = child.position.y;
                child.position.y = -swap;
            }, this);
        }
    },
    BlockControlDown: function(){
        
        var canMove = true;
        this.new_block.forEach(function (child) {
            //console.log(this.map[(child.position.y + this.new_block.position.y + 18) / 18][(child.position.x + this.new_block.position.x + 18) / 18]);
            var y = (child.position.y + this.new_block.position.y + 18) / 18;
            if (y < 0) y = 0;
            var x = (child.position.x + this.new_block.position.x) / 18;
            if (this.map[y][x] == 1) {
                canMove = false;
                return;
            }
        }, this);
        if(canMove) this.new_block.position.y += 18;
    },
    BlockControlDrop: function(){
        while(this.new_block.onLand == false){
            this.BlockFalling();
        }
        return this.new_block.position.y;
    },
    BlockFalling: function(){
        var canFall = true;
        //console.log(this.map);
        this.new_block.forEach(function(child){
            //console.log(this.map[(child.position.y + this.new_block.position.y + 18) / 18][(child.position.x + this.new_block.position.x + 18) / 18]);
            var y = (child.position.y + this.new_block.position.y + 18) / 18;
            if(y<0) y=0;
            //console.log(y);
            var x = (child.position.x + this.new_block.position.x ) / 18;
            if (this.map[y][x] == 1 ){
                this.new_block.onLand = true;
                canFall = false;
                return;
            }
        },this);
        if (canFall) {
            this.new_block.position.y += 18;
        }
    },
    checkClear: function(){
        var hit = 0;
        //console.log(this.obstacle);
        //console.log(this.map);
        //console.log(this.obastacle[28][5].world);
        for(var i=0;i<29;i++){
            if(this.map[i].indexOf(0) == -1){
                hit += 1;
                //console.log('find');
                for(var j=0;j<=9;j++){
                    this.obstacle[i][j].kill();
                    this.obstacle[i][j] = null;
                }
                this.map[i] = [0,0,0,0,0,0,0,0,0,0];
                for (var t = i; t > 0; t--) {
                    this.map[t] = this.map[t - 1];
                    for(var m=0;m<10;m++){
                        if(this.obstacle[t-1][m]){
                            this.obstacle[t-1][m].position.y += 18;
                        }
                    }
                    this.obstacle[t] = this.obstacle[t-1];
                }
            }
        }  
        if(hit!=0){
            this.playerAttack(hit);
        }
    },
    playerAttack: function(hit){
        game.add.tween(this.player).to({x:190},200).yoyo(true).start();
        game.add.tween(this.gabulin).to({
            tint: 0xff0000
        }, 200).easing(Phaser.Easing.Exponential.InOut).yoyo(true).start();
        if(hit == 1){
            console.log('hero deal three damage to monster');
            this.gabulin.life -= 3;
            console.log(this.gabulin.life+'/10');
        }
    },
    MonsterAutoAttack:function(){
        if(this.stage_now == 1){
            game.add.tween(this.gabulin).to({x:this.gabulin.position.x-10},200).yoyo(true).start();
            game.add.tween(this.player).to({
                tint: 0xff0000
            }, 150).easing(Phaser.Easing.Exponential.Out).yoyo(true).start();
            console.log('monster deal two damage to hero');
            this.player.life -= 2;
            console.log(this.player.life + '/15');
        }
    },
    checkPosition: function(){
        if(!this.check_block.length ){
            switch(this.number){
                case 0:
                    game.add.sprite(0, 0, this.block_array[this.number], 0, this.check_block);
                    game.add.sprite(-18, 0, this.block_array[this.number], 0, this.check_block);
                    game.add.sprite(-18, -18, this.block_array[this.number], 0, this.check_block);
                    game.add.sprite(18, 0, this.block_array[this.number], 0, this.check_block);
                    break;
                case 1:
                    game.add.sprite(0, 0, this.block_array[this.number], 0, this.check_block);
                    game.add.sprite(0, -18, this.block_array[this.number], 0, this.check_block);
                    game.add.sprite(-18, -18, this.block_array[this.number], 0, this.check_block);
                    game.add.sprite(18, 0, this.block_array[this.number], 0, this.check_block);
                    break;
                case 2:
                    game.add.sprite(0, 0, this.block_array[this.number], 0, this.check_block);
                    game.add.sprite(0, -18, this.block_array[this.number], 0, this.check_block);
                    game.add.sprite(0, -36, this.block_array[this.number], 0, this.check_block);
                    game.add.sprite(0, 18, this.block_array[this.number], 0, this.check_block);
                    break;
                case 3:
                    game.add.sprite(0, 0, this.block_array[this.number], 0, this.check_block);
                    game.add.sprite(-18, 0, this.block_array[this.number], 0, this.check_block);
                    game.add.sprite(18, -18, this.block_array[this.number], 0, this.check_block);
                    game.add.sprite(18, 0, this.block_array[this.number], 0, this.check_block);
                    break;
                case 4:
                    game.add.sprite(0, 0, this.block_array[this.number], 0, this.check_block);
                    game.add.sprite(-18, 0, this.block_array[this.number], 0, this.check_block);
                    game.add.sprite(0, -18, this.block_array[this.number], 0, this.check_block);
                    game.add.sprite(18, 0, this.block_array[this.number], 0, this.check_block);
                    break;
                case 5:
                    game.add.sprite(0, 0, this.block_array[this.number], 0, this.check_block);
                    game.add.sprite(-18, 0, this.block_array[this.number], 0, this.check_block);
                    game.add.sprite(0, -18, this.block_array[this.number], 0, this.check_block);
                    game.add.sprite(18, -18, this.block_array[this.number], 0, this.check_block);
                    break;
                case 6:
                    game.add.sprite(0, 0, this.block_array[this.number], 0, this.check_block);
                    game.add.sprite(-18, 0, this.block_array[this.number], 0, this.check_block);
                    game.add.sprite(-18, -18, this.block_array[this.number], 0, this.check_block);
                    game.add.sprite(0, -18, this.block_array[this.number], 0, this.check_block);
                    break;
            }
        }
        
        this.check_block.alpha=0.3;
        this.check_block.position.x = this.new_block.position.x;
        
        this.y_final = this.new_block.position.y;
        this.checkMove = true;
        while (this.checkMove) {
            this.new_block.forEach(function (child) {
                //console.log(this.map[(child.position.y + this.new_block.position.y + 18) / 18][(child.position.x + this.new_block.position.x + 18) / 18]);
                var y = (child.position.y + this.y_final + 18) / 18;
                if (y < 0) y = 0;
                var x = (child.position.x + this.new_block.position.x) / 18;
                if (this.map[y][x] == 1) {
                    this.checkMove = false;
                    return;
                }
            }, this);
            if (this.checkMove) this.y_final += 18;
        }
        this.check_block.position.y = this.y_final;
       //  console.log(this.check_block.length);
    },
    LoseGame: function(){
        game.camera.shake();
    }
};
